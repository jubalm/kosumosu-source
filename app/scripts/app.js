'use strict';

(function ($) {

  window.KOSU = angular
    .module('KOSU', [
      'ngAnimate',
      'ngCookies',
      'ngResource',
      'ngSanitize',
      'ngTouch',
      'ngRoute'
    ]);


})();

jQuery(function () {

  $('.logo__kosu').css({ opacity: 1 });

});
