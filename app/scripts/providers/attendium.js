'use strict';

(function (app) {

  app.factory('attendiumService', function($http, ATT_API){
    return {
      // TODO: add UUID to the url for REST API
      getUser: function() {
        return $http({
          method: 'GET',
          url: ATT_API.url //+ uuid
        });
      }
    };
  });

})(window.KOSU);
