'use strict';

window.onload = function(){
  var wrap = document.getElementsByClassName('particles')[0];
  window.canvas = document.createElement('canvas');
  var ctx = canvas.getContext("2d");
  window.imgz = new Image();
  imgz.src = '../images/bg-gradient.png';

  wrap.appendChild(canvas);

  canvas.width = wrap.offsetWidth;
  canvas.height= wrap.offsetHeight;

  var container = {
    x: 0, y: Math.floor(canvas.height * 0.25), width: canvas.width, height: Math.floor(canvas.height * 0.75)
  };
  var circles = [];
  var variableSpeed = 0.2;
  var circlesCount = 100;

  var getRandomInt = function(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  };

  var getRandomArbitrary = function(min, max) {
    return Math.random() * (max - min) + min;
  };

  var createCircles = function(){

    for(var i = 0; i < circlesCount; i++) {
      circles[i] = {};
      circles[i].r = getRandomInt(3, 8);
      circles[i].a = circles[i].r/25;
      circles[i].x = getRandomInt(circles[i].r, (container.x+container.width) - circles[i].r);
      circles[i].y = getRandomInt(circles[i].r, (container.y+container.height) - circles[i].r);
      circles[i].vx = getRandomArbitrary(-variableSpeed, variableSpeed);
      circles[i].vy = getRandomArbitrary(-variableSpeed, variableSpeed);
      circles[i].f = getRandomArbitrary(-1, 1);
    }

  };

  var renderCircles = function() {

    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    for(var i = 0; i < circles.length; i++) {

      ctx.strokeStyle = "black";
      ctx.save();
      ctx.beginPath();
      ctx.arc(circles[i].x, circles[i].y, circles[i].r, 0, Math.PI * 2, false);
      ctx.clip();
      ctx.drawImage(imgz, 0, 0);
      ctx.restore();
      ctx.stroke();
      ctx.globalAlpha = circles[i].a;

      if((circles[i].x + circles[i].vx + circles[i].r > container.x + container.width) || (circles[i].x - circles[i].r + circles[i].vx < container.x)){
        circles[i].vx = - circles[i].vx;
      }
      if((circles[i].y + circles[i].vy + circles[i].r > container.y + container.height) || (circles[i].y - circles[i].r + circles[i].vy < container.y)){
        circles[i].vy = - circles[i].vy;
      }
      circles[i].x +=circles[i].vx;
      circles[i].y +=circles[i].vy;

    }

    requestAnimationFrame(renderCircles);

  };


  window.draw = function () {

    createCircles();
    renderCircles();

  };

  draw();

};
