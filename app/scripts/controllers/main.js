'use strict';

/**
 * @ngdoc function
 * @name sourceWwwApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sourceWwwApp
 */
(function (app) {

  app.constant('ATT_API', {
    url: 'http://54.179.145.96/api/user/1/recipe/39/favorite'
  });

  app.controller('AppCtrl', function(){
    // Some app related stuff
  });

  app.controller('RsvpCtrl', function ($scope, attendiumService, $location) {

    $scope.rsvp = false;

    var validateUser = function(res){
      if ( String(res.id) === $location.search().uuid ) {
        $scope.rsvp = true;
      }
    };

    $scope.getRsvp = function() {
      return $scope.rsvp ? 'views/invited.html' : '';
    };

    attendiumService
      .getUser($location.search().uuid)
        .success(validateUser)
        .error(function(){
          window.alert('can\'t connect to api!');
        });
  });

  app.controller('InviteController', function($scope, $routeParams){
    console.log($routeParams);
  });

  app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {

    $locationProvider.html5Mode(true);

    $routeProvider.
      when('/', {
        templateUrl: 'views/main.html'
      }).
      when('/invite', {
        templateUrl: 'views/invite.html'
      }).
      when('/thanks', {
        templateUrl: 'views/thanks.html'
      }).
      otherwise({
        redirectTo: '/invite'
      });

  }]);



})(window.KOSU);
